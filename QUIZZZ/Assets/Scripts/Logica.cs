﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("Que nome aparece no guia de TV que tem no apartamento de Chandler e Joey?", 5);
        p1.AddResposta("Mr. Chandler Bing ", false);
        p1.AddResposta("Mrs. Chandler Bong", false);
        p1.AddResposta("Miss Chanandler Bong", true);
        p1.AddResposta("Mr.Chandler Bong", false);

        ClassPergunta p2 = new ClassPergunta("Que nome tem no barco de Joey?", 3);
        p2.AddResposta("The Mr. Beaumont", true);
        p2.AddResposta("Barco do Joey", false);
        p2.AddResposta("Chick and Duck", false);
        p2.AddResposta("Rosita", false);
        ClassPergunta p3 = new ClassPergunta("Qual é a profissão de Richard Burke?", 6);
        p3.AddResposta("Ortodontista", false);
        p3.AddResposta("Oftamologista", true);
        p3.AddResposta("Pediatra", false);
        p3.AddResposta("Origamista", false);

        ClassPergunta p4 = new ClassPergunta("Qual é o nome do meio de Chandler?", 2);
        p4.AddResposta("Murray", false);
        p4.AddResposta("Muriel", false);
        p4.AddResposta("Moby", true);
        p4.AddResposta("Ele não tem um sobrenome do meio", false);

        ClassPergunta p5 = new ClassPergunta("Quantas irmãs o Joey tem?", 5);
        p5.AddResposta("5", false);
        p5.AddResposta("6", false);
        p5.AddResposta("8", false);
        p5.AddResposta("7", true);

        ClassPergunta p6 = new ClassPergunta("Qual é o sobrenome do Tag (namorado da Rachel)?", 5);
        p6.AddResposta("Mitchell", false);
        p6.AddResposta("Jones", true);
        p6.AddResposta("Stevens", false);
        p6.AddResposta("Seu sobrenome jamais é revelado", false);

        ClassPergunta p7 = new ClassPergunta("Qual era a profissão do marido de Phoebe, antes que ele se tornasse um pianista?", 5);
        p7.AddResposta("Médico", false);
        p7.AddResposta("Advogado", true);
        p7.AddResposta("Bancário", false);
        p7.AddResposta("Assistente social", false);

        ClassPergunta p8 = new ClassPergunta("No episódio The One With Mrs Bing, quem beija a mãe do Chandler?", 5);
        p8.AddResposta("Ross", true);
        p8.AddResposta("Joey", false);
        p8.AddResposta("Um garçom", false);
        p8.AddResposta("Monica", false);

        ClassPergunta p9 = new ClassPergunta("Para qual time de basquete o Ross, o Joey e o Chandler torcem ?", 5);
        p9.AddResposta("New York Knicks", true);
        p9.AddResposta("Los Angeles Lakers", false);
        p9.AddResposta("Chicago Bulls", false);
        p9.AddResposta("Sacramento Kings", false);

        ClassPergunta p10 = new ClassPergunta("Qual é o nome do pinguim de pelúcia do Joey ? ", 5);
        p10.AddResposta("Huggy", false);
        p10.AddResposta("Hugger", false);
        p10.AddResposta("Hugsy", true);
        p10.AddResposta("Huggable", false);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
